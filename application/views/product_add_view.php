<main>
    <form action="" method="">
        <label>Product name:
            <input type="text" name="name" required="required">
        </label><br>
        <label>Product category:
            <select name="category">
                <?php foreach ($category as $cat): ?>
                    <option value="<?php echo $cat['id']; ?>"><?php echo $cat['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </label><br>
        <label>Product type:
            <select name="type">
                <?php foreach ($type as $typ): ?>
                    <option value="<?php echo $typ['id']; ?>"><?php echo $typ['name'] ?></option>
                <?php endforeach; ?>
            </select>
        </label><br>
        <label>Product description:
            <textarea name="description"></textarea>
        </label><br>
        <input type="file">
    </form>
    <button class="add">Add</button>
    
</main>
</html>
<script>
    function Success(data) {
        alert(data);
    }
    $(document).ready(function () {
        $('.add').click(function () {
            var name = $('[name = name]').val();
            var category = $('[name = category]').val();
            var type = $('[name = type]').val();
            var description = $('[name = description]').val();
            if (name === '') {
                alert("Please, enter name");
            } else if (description === "") {
                alert("Please, enter description");
            } else {
                $.ajax({
                    type: "POST",
                    url: "test2/index.php/ajax/productAdd",
                    dataType: "text",
                    data: ({"name": name,
                        "category": category,
                        "type": type,
                        "description": description}),
                    success: Success()

                });
            }
        });

    });
</script>