<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ProductsModel');
    }

    public function index() {

        $this->load->view('nav_view');
        $data['products'] = $this->ProductsModel->getall();
        $this->load->view('all_products_view', $data);
    }

    public function item(int $id) {

        $this->load->view('nav_view');
        $data['item'] = $this->ProductsModel->getitem($id);
        $this->load->view('item_view', $data);
    }

    public function add() {
        $data['category'] = $this->ProductsModel->getcategory();
        $data['type'] = $this->ProductsModel->gettype();
        $this->load->view('nav_view');
        $this->load->view('product_add_view',$data);
    }

}
