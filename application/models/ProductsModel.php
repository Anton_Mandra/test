<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProductsModel extends CI_Model {

    public function getall() {
        
        $products = $this->db
                ->select('p.id, p.name, c.name cat_name, pt.name type_name')
                ->from('Product p')
                ->join('Category c', 'p.category_id = c.id', 'LEFT')
                ->join('Product_type pt', 'p.product_type_id=pt.id', 'LEFT')
                ->get()
                ->result_array();
        return $products;

    }
public function getitem($id) {
        
        $item = $this->db
                ->select('p.*, p.name, c.name cat_name, pt.name type_name')
                ->from('Product p')
                ->join('Category c', 'p.category_id = c.id', 'LEFT')
                ->join('Product_type pt', 'p.product_type_id=pt.id', 'LEFT')
                ->where('p.id',$id)
                ->get()
                ->row_array();
        return $item;

    }
   public function getcategory() {
        
        $category = $this->db
                ->select('*')
                ->from('Category')
                ->get()
                ->result_array();
        return $category;

    }
    public function gettype() {
        
        $type = $this->db
                ->select('*')
                ->from('Product_type')
                ->get()
                ->result_array();
        return $type;

    }
}
